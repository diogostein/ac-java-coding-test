package models;

import java.util.LinkedHashMap;
import java.util.Map;

public class Persona {

	private String name;
	private Map<String, Integer> countWords;
	
	public Persona() {
		countWords = new LinkedHashMap<>();		
	}	
	
	public Persona(String name) {
		countWords = new LinkedHashMap<>();	
		this.name = name;
	}	
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setCountWords(String word) {	
		int i = countWords.containsKey(word) ? countWords.get(word) + 1 : 1;
		countWords.put(word, i);
	}
	
	public String getName() {
		return this.name;
	}
	
	public Map<String, Integer> getCountWords() {
		return this.countWords;
	}
}
