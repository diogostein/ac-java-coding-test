package models;

import java.util.LinkedHashSet;
import java.util.Set;

public class Setting {
	
	private String name;
	private Set<String> characters;
	
	public Setting() {
		characters = new LinkedHashSet<String>();		
	}	
	
	public Setting(String name) {
		characters = new LinkedHashSet<String>();		
		this.name = name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setCharacter(String character) {
		this.characters.add(character);
	}
	
	public String getName() {
		return this.name;
	}
	
	public Set<String> getCharacters() {
		return this.characters;
	}

}
