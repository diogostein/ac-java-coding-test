package utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.google.common.base.Splitter;

import constants.Const;

public class Utils {
	
	public static String formatAsHeader(String s) {
		return String.format("# %s", s);
	}
	
	public static String formatAsContent(String s) {
		return String.format("* %s", s);
	}
	
	public static boolean isAllUpperCaseCharacterLine(String sLine) {
		char[] letters = sLine.trim().toCharArray();		
		for(char letter : letters) {
			if(Character.isLetterOrDigit(letter) && !Character.isUpperCase(letter)) {
				return false;
			}
		}		
		return true;
	}
	
	public static List<String> cleanDialogLineToWords(String s) {		
		return Splitter.onPattern(Const.REGEX_CLEAN_DIALOG_LINE)
				.omitEmptyStrings()
				.splitToList(s.trim().toLowerCase());
	}
	
	public static String cleanSettingLine(String s) {
		return Splitter.onPattern(Const.REGEX_CLEAN_SETTING_LINE).omitEmptyStrings().splitToList(s).get(0);
	}
	
	public static void writeToFile(String content, String fileName) {
		try {	
			File file = new File(fileName);
			if (!file.exists()) file.createNewFile();	
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static Map<String, Integer> getTopTenWords(Map<String, Integer> map) {		
		Map<String, Integer> sortedMap = new TreeMap<>(new ValueComparator(map));		
		sortedMap.putAll(map);
		
		int i = 1;
		Iterator<Map.Entry<String, Integer>> it = sortedMap.entrySet().iterator();
		while(it.hasNext()) {
			it.next();
			if(i++ > Const.TOP_MAX_RESULTS) it.remove();	        
		}
		
		return sortedMap;
	}
}
