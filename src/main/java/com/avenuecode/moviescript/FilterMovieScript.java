package com.avenuecode.moviescript;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import utils.Utils;
import models.Persona;
import models.Setting;

public class FilterMovieScript {
	
	int count;

	private Map<String, Setting> settings;
	private Map<String, Persona> characters;
	private String currentSetting;
	private String currentCharacter;
	
	public FilterMovieScript() {
		settings = new LinkedHashMap<>();
		characters = new LinkedHashMap<>();
	}	
	
	public void formatAndSaveSetting(String sLine) {
		String settingName = Utils.cleanSettingLine(sLine);
        if(!settings.containsKey(settingName)) {
			settings.put(settingName, new Setting(settingName));
		}
        currentSetting = settingName;       
	}
	
	public void formatAndSaveCharacter(String sLine) {
		String sCharacter = sLine.trim();
		settings.get(currentSetting).setCharacter(sCharacter);
		if(!characters.containsKey(sCharacter)) {
			characters.put(sCharacter, new Persona(sCharacter));
		}
		currentCharacter = sCharacter;
	}
	
	public void formatAndSaveDialog(String sLine) {		
		List<String> words = Utils.cleanDialogLineToWords(sLine);		
		for(String word : words) {				
			characters.get(currentCharacter).setCountWords(word);
		}		
	}	
	
	public void printOnFile(String outputFile) {
		StringBuilder output = new StringBuilder();
		for(Setting setting : settings.values()) {			
			output.append(String.format("%s\n\n", Utils.formatAsHeader(setting.getName())));				
			for(String character : setting.getCharacters()) {				
				output.append(String.format("%s\n", Utils.formatAsContent(character)));				
			}
			output.append("\n");			
		}
		for(Persona character : characters.values()) { 					
			output.append(String.format("%s\n\n", Utils.formatAsHeader(character.getName())));				
			Map<String, Integer> topTenWords = Utils.getTopTenWords(character.getCountWords());			
			for(Map.Entry<String, Integer> countWord : topTenWords.entrySet()) {				
				output.append(String.format("%s\n", Utils.formatAsContent(String.format("%s: %s", countWord.getKey(), countWord.getValue()))));
			}			
			output.append("\n");
        }	
		Utils.writeToFile(output.toString(), outputFile);
	}
}
