package com.avenuecode.moviescript;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.apache.commons.lang.StringUtils;

import utils.Utils;
import constants.Const;

public class Init {
	
	private FilterMovieScript fms;
	
	private String inputFile;
	private String outputFile;
	
	public Init(String inputFile, String outputFile) {
		fms = new FilterMovieScript();
		this.inputFile = inputFile;
		this.outputFile = outputFile;		
		processFile();
	}
	
	private void processFile() {
		File file = new File(inputFile);
		try {
			Scanner in = new Scanner(file);
			while(in.hasNext()) {
				doFilter(in.nextLine());				
			}
			in.close();				
			fms.printOnFile(outputFile);
		} catch(FileNotFoundException error) {
			System.out.println("Input file not found!");
		} catch(Exception error) {
			System.out.println("Error loading the file. Check if it's a movie script text file.");
		}
	}
	
	private void doFilter(String sLine) {
		if(!StringUtils.isBlank(sLine)) {			
			if(isSettingLine(sLine)) {
				fms.formatAndSaveSetting(sLine);
			} else if(isCharacterLine(sLine)) {
				fms.formatAndSaveCharacter(sLine);
			} else if(isCharacterDialogLine(sLine)) {
				fms.formatAndSaveDialog(sLine);
			}
		}		
	}
	
	private boolean isSettingLine(String sLine) {		
		return sLine.startsWith(Const.INT) || sLine.startsWith(Const.EXT) || sLine.startsWith(Const.INT_EXT);
	}
	
	private boolean isCharacterLine(String sLine) {
		return sLine.indexOf(sLine.trim()) == Const.CHAR_INDENT 
				&& Utils.isAllUpperCaseCharacterLine(sLine);
	}
	
	private boolean isCharacterDialogLine(String sLine) {
		return sLine.indexOf(sLine.trim()) == Const.DIALOG_INDENT;
	}	
}
