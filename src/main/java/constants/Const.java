package constants;

public class Const {
	
	public static final String INT = "INT.";
	public static final String EXT = "EXT.";
	public static final String INT_EXT = "INT./EXT.";
	
	public static final int CHAR_INDENT = 22;
	public static final int DIALOG_INDENT = 10;
	
	public static final int TOP_MAX_RESULTS = 10;
	
	public static final String REGEX_CLEAN_SETTING_LINE = "INT. |EXT. |INT./EXT. | -|\\.";
	public static final String REGEX_CLEAN_DIALOG_LINE = " |\\.|\\?|\\!|\\,|\\:|\\;|\\...|-|\"";

}
