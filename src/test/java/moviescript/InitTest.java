package moviescript;

import static org.junit.Assert.*;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import constants.Const;

public class InitTest {
	
	private String[] settingLines = {
			"EXT. TATOOINE - DESERT WASTELAND - DAY", 
			"INT. REBEL BLOCKADE RUNNER - MAIN HALLWAY", 
		    "INT./EXT. LUKE'S SPEEDER - DESERT WASTELAND - TRAVELING - DAY"
	};		
	private String narrativeLine = "The awesome yellow planet of Tatooine emerges from a total";
	private String characterLine = "                      THREEPIO";
	private String dialogLine = "          Did you hear that? They've shut down";
	private String dialogDescriptionLine = "               (laugh)";

	@Test
	public void checkIfItIsASettingLine() {		
		assertTrue(isSettingLine(settingLines[0]));
		assertTrue(isSettingLine(settingLines[1]));
		assertTrue(isSettingLine(settingLines[2]));
		assertFalse(isSettingLine(narrativeLine));
		assertFalse(isSettingLine(characterLine));
		assertFalse(isSettingLine(dialogLine));
		assertFalse(isSettingLine(dialogDescriptionLine));
	}
	
	@Test
	public void checkIfItIsACharacterLine() {		
		assertFalse(isCharacterLine(settingLines[0]));
		assertFalse(isCharacterLine(settingLines[1]));
		assertFalse(isCharacterLine(settingLines[2]));
		assertFalse(isCharacterLine(narrativeLine));
		assertTrue(isCharacterLine(characterLine));
		assertFalse(isCharacterLine(dialogLine));
		assertFalse(isCharacterLine(dialogDescriptionLine));
	}
	
	@Test
	public void checkIfItIsACharacterDialogLine() {		
		assertFalse(isCharacterDialogLine(settingLines[0]));
		assertFalse(isCharacterDialogLine(settingLines[1]));
		assertFalse(isCharacterDialogLine(settingLines[2]));
		assertFalse(isCharacterDialogLine(narrativeLine));
		assertFalse(isCharacterDialogLine(characterLine));
		assertTrue(isCharacterDialogLine(dialogLine));
		assertFalse(isCharacterDialogLine(dialogDescriptionLine));
	}
	
	private boolean isSettingLine(String sLine) {		
		return sLine.startsWith(Const.INT) || sLine.startsWith(Const.EXT) || sLine.startsWith(Const.INT_EXT);
	}
	
	private boolean isCharacterLine(String sLine) {
		return sLine.indexOf(sLine.trim()) == Const.CHAR_INDENT 
				&& StringUtils.isAllUpperCase(sLine.trim().replace(" ", ""));
	}
	
	private boolean isCharacterDialogLine(String sLine) {
		return sLine.indexOf(sLine.trim()) == Const.DIALOG_INDENT;
	}	
}
