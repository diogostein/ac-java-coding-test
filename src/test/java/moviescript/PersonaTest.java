package moviescript;

import static org.junit.Assert.*;

import models.Persona;

import org.junit.Test;

public class PersonaTest {

	@Test
	public void checkIfWordsOfCharacterIsBeingCounted() {
		Persona character = new Persona("Han");
		character.setCountWords("hello");
		character.setCountWords("hello");
		character.setCountWords("captain");
		character.setCountWords("the");
		character.setCountWords("to");
		character.setCountWords("hello");
		character.setCountWords("passage");
		character.setCountWords("the");		
		
		String expected = "{hello=3, captain=1, the=2, to=1, passage=1}";
		String actual = character.getCountWords().toString();
		
		assertEquals("should return same sum for each word", expected, actual);
	}
}
