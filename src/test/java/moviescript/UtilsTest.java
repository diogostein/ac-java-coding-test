package moviescript;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import utils.Utils;

public class UtilsTest {

	@Test
	public void splitToWordsAndCleanDialogLine() {		
		String testLine = " ...Hi-Hi, --- my friend. Aren't you going to the \"Area 51\"? Yes: Going!;";		
		String[] testWords = {"hi", "hi", "my", "friend", "aren't", "you", "going", "to", "the", "area", "51", "yes", "going"};
		
		List<String> expected = Arrays.asList(testWords);
		List<String> actual = Utils.cleanDialogLineToWords(testLine);
		
		assertEquals("should contain same list of cleaned words", expected, actual);		
	}
	
	@Test
	public void cleanSettingLine() {		
		String[] expected = {"TATOOINE", "REBEL BLOCKADE RUNNER", "LUKE'S SPEEDER"};
		String[] actual = {
				Utils.cleanSettingLine("EXT. TATOOINE - DESERT WASTELAND - DAY"), 
				Utils.cleanSettingLine("INT. REBEL BLOCKADE RUNNER - MAIN HALLWAY"), 
				Utils.cleanSettingLine("INT./EXT. LUKE'S SPEEDER - DESERT WASTELAND - TRAVELING - DAY")
		};		
		
		assertArrayEquals("should contain same array of cleaned settings", expected, actual);		
	}
	
	@Test
	public void getTopTenWordsByValueOfTheMap() {
		String[][] testCountedWords = {
				{"dog","98"},{"cat","87"},{"lion","64"},{"snake","58"},{"rabbit","43"},{"bird","34"},
				{"bear","34"},{"giraffe", "30"},{"tiger","22"},{"elephant","17"},{"rhino","13"},{"gorrilla","8"}			
		};
		
		Map<String, Integer> expected = new LinkedHashMap<>();
		for(int i = 0; i < 10; i++) {
			expected.put(testCountedWords[i][0], Integer.parseInt(testCountedWords[i][1]));
		}
		
		Map<String, Integer> actual = new HashMap<>();
		for(int i = testCountedWords.length - 1; i >= 0; i--) {
			actual.put(testCountedWords[i][0], Integer.parseInt(testCountedWords[i][1]));
		}
		actual = Utils.getTopTenWords(actual);
		
		assertEquals("should contain same first entry \"dog\"", 
				expected.entrySet().iterator().next().getKey(), 
				actual.entrySet().iterator().next().getKey()
		);	
		assertEquals("should contain only 10 entries", 10, actual.size());
	}
	
}
